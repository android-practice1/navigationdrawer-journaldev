# Navigation Drawer

Android Navigation Drawer is a sliding left menu that is used to display the important links in the application. Navigation drawer makes it easy to navigate to and fro between those links. It’s not visible by default and it needs to opened either by sliding from left or clicking its icon in the **ActionBar**.

> ## Note
> This code is **taken/copyed/implemented** from [Journal Dev](https://www.journaldev.com/9958/android-navigation-drawer-example-tutorial) website. If you look closer, you'll see their code is from 2015 which is almost 6 years old! So, I've just updated and modified some of codes. 

### Screenshot 
![title](app_shot.png)

## Algorithm 

**Class List :**
  1. ConnectFragment
  2. DataModel
  3. DrawerItemCustomAdapter
  4. FixturesFragment
  5. MainActivity
  6. TableFragment



**DataModel**
  * no idea what the usage is 

**DrawerItemCustomAdapter**
  * A Custom Adapter 

**MainActivity**
  * Layout - activity_main
  * Method List :
    * onCreate
    * onOptionsItemSelected For *menu* item
    * selectItem
    * setTitle
    * onPostCreate
    * setupToolbar for scrollview 
    * setupDrawerToggle

**FixturesFragment**
  * Layout - fragment_fixtures
  * a activity

**TableFragment**

  * Layout - fragment_table
  * a activity 

**ConnectFragment**
  * Layout - fragment_connect
  * just a activity

**Extra Layouts :**
  * res/layout/toolbar.xml
  * res/layout/list_view_item_row.xml for list view in **adapter**
  * res/menu/menu_main.xml for menu in action bar 

## Usage 
 * No extra library has been used 
 * No extra device permission is required for this project! 



## Changelogs

  * Git Init 
  * Removed old assests 
  * Updated gradle build and gradle properties 
  * Removed all of v7 libraryes 
  * Added Algorithm data in readme file 



### Source

[Journal Dev](https://www.journaldev.com/9958/android-navigation-drawer-example-tutorial)